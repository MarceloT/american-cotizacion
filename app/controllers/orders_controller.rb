class OrdersController < ApplicationController
  before_action :authenticate_user! 
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  before_action :get_all_exams, only: [:new, :edit, :create, :update]
  before_action :get_all_patients, only: [:new, :edit, :create, :update]
  skip_before_filter :verify_authenticity_token, :only => [:search]

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all.page(params[:page]).per(20)
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
    @order.date = Time.now
    @order.order_items.build unless @order.order_items.any?
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)

    @order.exam_number = 0
    num_no_exam = 0
    Rails.logger.info "=== order_params[:order_items_attributes].count: #{order_params[:order_items_attributes].count}"
    if order_params[:order_items_attributes].present?
      order_params[:order_items_attributes].map{|q| num_no_exam  += 1 if q[1][:exam_id] == '' }
      @order.exam_number = order_params[:order_items_attributes].count - num_no_exam
    end
    Rails.logger.info "=== @order.order_items.count: #{@order.order_items.count}"
    respond_to do |format|
      if @order.save

        # order_params[:order_items_attributes].each do |item|
        #   i = OrderItem.new
        #   i.order_id = @order.id
        #   i.exam_id = item[1][:exam_id]
        #   i.save if item[1][:exam_id].present?
        # end

        format.html { redirect_to @order, notice: 'Orden creado con éxito.' }
        format.json { render :show, status: :created, location: @order }
      else
        @order.order_items.build unless @order.order_items.any?
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    @order.exam_number = 0
    num_no_exam = 0
    if order_params[:order_items_attributes].present?
      order_params[:order_items_attributes].map{|q| num_no_exam  += 1 if (q[1][:exam_id] == '' || q[1][:_destroy] == '1')}
      @order.exam_number = order_params[:order_items_attributes].count - num_no_exam
    end
    respond_to do |format|
      if @order.update(order_params)

        format.html { redirect_to @order, notice: 'Orden actualizada con éxito.' }
        format.json { render :show, status: :ok, location: @order }
      else
        if @order.exam_number == 0
          @order.order_items = []
          @order.order_items.build
        end
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Orden elimindad con éxito.' }
      format.json { head :no_content }
    end
  end

  def search
    # params[:page] = 1 if params[:page].nil?
    patients = Patient.where("name ILIKE ? OR last_name ILIKE ? OR ci ILIKE ?", "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%")

    @orders = Order.order(:created_at).reverse_order
    @orders = @orders.where("patient_id IN (?)", patients.map{|p| p.id}).page(params[:page]).per(12)
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:patient_id, :date, :description, :date_approved, :date_closed, :pdf, :paid, :priority, :pdf_upload, :total, :diagnosis, :note, order_items_attributes: [:id, :exam_id, :order_id, :_destroy])
    end
end
