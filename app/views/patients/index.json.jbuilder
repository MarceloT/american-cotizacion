json.array!(@patients) do |patient|
  json.extract! patient, :id, :ci, :name, :last_name, :gener, :age, :phone, :email, :direction
  json.url patient_url(patient, format: :json)
end
