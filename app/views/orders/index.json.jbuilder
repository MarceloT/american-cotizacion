json.array!(@orders) do |order|
  json.extract! order, :id, :date, :description, :date_approved, :date_closed, :pdf, :paid, :priority, :total, :diagnosis, :note
  json.url order_url(order, format: :json)
end
