json.array!(@tubes) do |tube|
  json.extract! tube, :id, :code, :name, :price
  json.url tube_url(tube, format: :json)
end
