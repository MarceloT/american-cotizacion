class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  devise :database_authenticatable, :rememberable, :trackable, :recoverable, :registerable
  #Validations
  validates_presence_of :login
  validates :login, uniqueness: true
  validates_presence_of :password, :password_confirmation, on: :create
  
  has_many :companies

  class << self
	  def get_name_by_id(id)
	  	User.find(id).name
	  end
    def can_edit(user)
      if user.rol == 3
        true
      else
        false
      end
    end
	end

  def role
    if self.rol == 0
      role = "medic"
    elsif self.rol == 1
      role = "labor"
    elsif self.rol == 2
      role = "recep"
    elsif self.rol == 3
      role = "admin"
    end  
    role
  end

  def role_fullname
    if self.rol == 0
      role_fullname = "Médico"
    elsif self.rol == 1
      role_fullname = "Laboratorista"
    elsif self.rol == 2
      role_fullname = "Recepcionista"
    elsif self.rol == 3
      role_fullname = "Administrador"
    end  
    role_fullname
  end

end
