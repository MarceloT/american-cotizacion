class Patient < ActiveRecord::Base
	validates :ci, :presence => true
	validates :ci, uniqueness: true
	validate :ci_passport
	validates :name, :presence => true
	validates :last_name, :presence => true


	def ci_passport
		require "id_ecuador"
		ci = self.ci
		if ci.present?
			# Passport validation
			if passport.present? && ci.length != 9
				errors.add(:base, "Pasaporte inválido")
			elsif !passport.present?
				cedula = IdEcuador.new ci
				if !cedula.valid? || !ci.scan(/\D/).empty?
					errors_mjs = ci.scan(/\D/).empty? ? cedula.errors.join(", ") : "Solo se acepta valores numéricos"
					errors.add(:base, "Cédula inválida: #{errors_mjs}")
				end
			end
			
		end
	end

	def full_name
		self.last_name.present? ? "#{self.last_name} #{self.name}" : "#{self.name}"
	end

	def full_name_ci
		full_name = self.last_name.present? ? "#{self.last_name} #{self.name}" : "#{self.name}"
		full_name = "#{full_name} - #{self.ci}" if self.ci.present?
	end


	def gener_fullname
		if self.gener.present?
			[["Masculino","masculino"],["Femenino","femenino"],["Otros","otros"]]  
			if self.gener == "masculino"
				"Masculino"
			elsif self.gener == "femenino"
				"Femenino"
			elsif self.gener == "otros"
				"Otros"
			else
				"N/A"
			end
		else
			"N/A"
		end
	end

end
