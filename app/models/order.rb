class Order < ActiveRecord::Base
	belongs_to :patient
	has_many :order_items, :dependent => :destroy
	accepts_nested_attributes_for :order_items, reject_if: proc { |attributes| attributes['exam_id'].blank?}, allow_destroy: true

	validates :exam_number, :numericality => { :greater_than => 0}
	validates :patient_id, :presence => true

	def status
		order = self
		if order.pdf_upload && !order.paid
			"Resultados ajuntos"
		elsif order.paid && order.pdf_upload
			"Pagado"
		elsif order.diagnosis.present?
			"Finalizado"
		elsif !order.pdf_upload && !order.paid && !order.diagnosis.present?
			"Solicitud registrada"	
		else
			"No aplica"
		end
	end


	def can_edit(role_id)
		order = self
		can_edit = false
		if role_id == 0 && order.paid && order.pdf_upload
			# Médico
			can_edit = true
		elsif role_id == 1 && !order.paid && !order.diagnosis.present? && !order.pdf_upload
			# Laboratorista
			can_edit = true
		elsif role_id == 2 && order.pdf_upload
			# Recepcionista
			can_edit = true
		elsif role_id == 3
			# Administrador
			can_edit = true
		end
		can_edit
	end

	class << self
		def approved
			where.not(:diagnosis => ["", nil])
		end
		def pendig
			where(:pdf_upload => true, :paid => [false, nil])
		end
		def registered
			where(:pdf_upload => [false, nil], :paid => [false, nil], :diagnosis => ["", nil])
		end
	end

end
