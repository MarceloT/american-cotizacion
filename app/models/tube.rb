class Tube < ActiveRecord::Base
	validates :code, :presence => true, :uniqueness => true
	validates :name, :presence => true, :uniqueness => true
	
	class << self
		def can_edit(user)
			if user.rol == 3
				true
			else
				false
			end
		end
	end

	def activated
		true
	end

	def full_name
		self.price ? "#{self.name} ($ #{ActionController::Base.helpers.number_with_precision( self.price, :precision => 2)})" : "#{self.name}"
	end
end
