class AddPatientIdToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :patient_id, :integer
  	add_column :orders, :exam_number, :integer
  	add_column :orders, :pdf_upload, :boolean
  end
end
