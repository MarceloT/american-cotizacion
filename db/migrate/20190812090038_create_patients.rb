class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :ci
      t.string :name
      t.string :last_name
      t.string :gener
      t.integer :age
      t.string :phone
      t.string :email
      t.string :direction

      t.timestamps null: false
    end
  end
end
