class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.datetime :date
      t.string :description
      t.datetime :date_approved
      t.datetime :date_closed
      t.string :pdf
      t.boolean :paid
      t.boolean :priority
      t.decimal :total
      t.string :diagnosis
      t.string :note

      t.timestamps null: false
    end
  end
end
