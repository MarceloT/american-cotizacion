class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :exam_id
      t.string :description

      t.timestamps null: false
    end
  end
end
